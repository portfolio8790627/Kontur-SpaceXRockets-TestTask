# iOS Developer Test Task
Test assignment for an internship at the Kontur company  
The original description of this task [here](https://github.com/Arctanyn/Kontur-TestTask/files/10716439/iOS.pdf)  

## Preview
![IMG_3996](https://user-images.githubusercontent.com/81229461/229378098-11b80104-c7f8-4573-89d2-2fca7daffc3d.png)

## Stack
- iOS 13.0+  
- Swift, UIKit, MVP+Coordinator, DI

## Implementation (Max Level)

### Screen 1:
:white_check_mark: All the features on the slide of Screen 1  
:white_check_mark: The settings icon and navigation on the Settings screen (Screen 3)  
:white_check_mark: The ability to switch units of measurement for height, diameter, mass and payload parameters  

### Screen 2
:white_check_mark: All the features on the slide of Screen 2

### Screen 3
:white_check_mark: All the features on the slide of Screen 3
