//
//  Int + Ext.swift
//  Kontur-TestTask
//
//  Created by Малиль Дугулюбгов on 11.02.2023.
//

import Foundation

extension Int {
    func currencyToString() -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "en_US")
        formatter.currencySymbol = "$"
        formatter.maximumFractionDigits = 1
        

        switch self {
        case 1_000_000_000..<1_000_000_000_000:
            return formatter.string(from: NSNumber(value: Double(self) / 1_000_000_000))! + "B"
        case 1_000_000..<1_000_000_000:
            return formatter.string(from: NSNumber(value: Double(self) / 1_000_000))! + "M"
        default:
            return formatter.string(from: NSNumber(value: self))!
        }
    }
}

